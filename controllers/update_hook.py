import base64
import openerp.addons.web.http as oeweb
from openerp.addons.web.controllers.main import content_disposition
from openerp.modules.registry import RegistryManager
import logging
logger = logging.getLogger(__name__)
import json

class GitController(oeweb.Controller):
    _cp_path = "/git"
    @oeweb.httprequest
    def update(self, req, **kw):
        logger.info("params %s" % kw)
        payload = kw.get('payload', False)
        jsondata = json.loads(payload)
        project_name = jsondata.get('repository').get('name', False)
        project_owner = jsondata.get('owner').get('name', False)
        if not project_name and not project_owner:
            logger.info("failed to update project")
            return "Failed to query project name/owner"
        registry = RegistryManager.get(project_owner)
        with registry.cursor() as cr:
            project_id = registry.get("project.project").search(cr,1,[('name','ilike', project_name)])
            project_browse= registry.get("project.project").browse(cr,1, project_id)
            logger.info("projects %s" % project_id)
            for project in project_browse:
                logger.info("project %s" % project.name)
                project.get_git_repo()
        logger.info("project successfully updated")
        return "Success" 
